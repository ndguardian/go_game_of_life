package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	width  = 80
	height = 15
)

type universe [][]bool

func newUniverse() universe {
	universe := make([][]bool, height)

	for rowNum := range universe {
		universe[rowNum] = make([]bool, width)
	}

	return universe
}

func show(u universe) {
	for _, row := range u {
		printRow(row)
	}
}

func printRow(row []bool) {
	representation := make([]string, width)
	for i, cell := range row {
		if cell {
			representation[i] = "*"
		} else {
			representation[i] = " "
		}
	}

	fmt.Println(representation)
}

func seedUniverse(u universe) {
	for _, row := range u {
		seedRow(row)
	}
}

func seedRow(row []bool) {
	for cell := range row {
		row[cell] = seedCell()
	}
}

func seedCell() bool {
	return rand.Int31n(4)%4 == 0
}

func buildUpdatedUniverse(u universe) universe {
	updatedUniverse := newUniverse()

	for y, row := range u {
		updatedUniverse[y] = buildUpdatedRow(row, y, u)
	}

	return updatedUniverse
}

func buildUpdatedRow(row []bool, y int, u universe) []bool {
	updatedRow := make([]bool, width)
	for x := range row {
		updatedRow[x] = checkIfCellShouldLive(x, y, u)
	}

	return updatedRow
}

func checkIfCellShouldLive(x int, y int, u universe) bool {
	liveNeighbors := getLiveNeighborCells(x, y, u)
	if u[y][x] && liveNeighbors == 2 {
		return true
	} else if liveNeighbors == 3 {
		return true
	} else {
		return false
	}
}

func getLiveNeighborCells(x int, y int, u universe) int {
	liveNeighbors := 0
	xIncrement := getXIncrement(x)
	xDecrement := getXDecrement(x)
	yIncrement := getYIncrement(y)
	yDecrement := getYDecrement(y)

	if u[y][xDecrement] {
		liveNeighbors++
	}

	if u[yDecrement][x] {
		liveNeighbors++
	}

	if u[y][xIncrement] {
		liveNeighbors++
	}

	if u[yIncrement][x] {
		liveNeighbors++
	}

	return liveNeighbors
}

func getXIncrement(x int) int {
	if x == width-1 {
		return 0
	}

	return x + 1
}

func getXDecrement(x int) int {
	if x == 0 {
		return width - 1
	}

	return x - 1
}

func getYIncrement(y int) int {
	if y == height-1 {
		return 0
	}

	return y + 1
}

func getYDecrement(y int) int {
	if y == 0 {
		return height - 1
	}

	return y - 1
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	universe := newUniverse()
	seedUniverse(universe)
	show(universe)

	for i := 0; i < 20; i++ {
		print("\033[H\033[2J")
		universe = buildUpdatedUniverse(universe)
		show(universe)
	}
	//fmt.Println("Updated version...")
	//show(universe)
}
